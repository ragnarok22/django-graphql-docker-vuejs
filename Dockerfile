FROM python:slim

WORKDIR /data

COPY django_project/requirements.txt django_project/requirements.txt
RUN pip install -r django_project/requirements.txt

COPY . .

ENTRYPOINT ["python"]
CMD ["django_project/manage.py", "runserver", "0.0.0.0:80"]