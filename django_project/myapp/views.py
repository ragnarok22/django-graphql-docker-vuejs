from django.views import View
from django.http import JsonResponse


class IndexView(View):
    def get(self, request):
        data = {
            'message': 'hello world!'
        }
        return JsonResponse(data)
